#!/bin/bash

mvn deploy:deploy-file \
  -DgroupId=io.craigmiller160 \
  -DartifactId=oauth2-utils-core \
  -Dversion=1.2.0-SNAPSHOT \
  -Dpackaging=jar \
  -Dfile=./target/oauth2-utils-core-1.2.0-SNAPSHOT.jar \
  -Dsources=./target/oauth2-utils-core-1.2.0-SNAPSHOT-sources.jar \
  -DgeneratePom=true \
  -DrepositoryId=nexus \
  -Durl=https://nexus.craigmiller160.us/repository/maven-snapshots